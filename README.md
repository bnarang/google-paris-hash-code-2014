# README #


```
#!
         ####  ####
        ####  ####
    ################
   ###############
     ####  ####
  ###############
 ###############
   ####  ####
  ####  ####

 Google Paris Hash Code - 4 et 5 avril 2014
 Team ENSTAckoverflow
 Quentin B. - Sylvain F. - Adrien L. - Benjamin N.
```

## Tâches

Les instructions et données d'entrée sont disponibles ici : [https://sites.google.com/site/hashcode2014/tasks](https://sites.google.com/site/hashcode2014/tasks).

## Dossiers

Les deux « rounds » sont présents dans des dossiers séparés.