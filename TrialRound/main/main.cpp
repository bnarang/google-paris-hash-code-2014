/*
 *
 *        ####   ####
 *       ####   ####
 *    ################
 *   ###############
 *     ####  ####
 *  ###############
 * ###############
 *   ####  ####
 *  ####  ####
 *
 * Google HashCode - 4 et 5 avril 2014
 *
 * Team ENSTAckoverflow
 * Quentin B. - Sylvain F. - Adrien L. - Benjamin N.
 *
 *
 * main.cpp
 *
 */

#include <boost/algorithm/string.hpp>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "functions.hpp"

using namespace std;
using namespace boost;

void read_input(const char* filename, vector< vector<int> > &v)
{
  vector<string> temp;
  vector<int> temp2;
  int height,width;
  string line;
  ifstream myfile (filename);

  if (myfile.is_open())
  {
    // Read dimension
    getline (myfile,line);
    split( temp, line, is_any_of( " " ), token_compress_on );

    height = atoi(temp[0].c_str());
    width = atoi(temp[1].c_str());

    while ( getline (myfile,line) )
    {
      temp2.clear();
      for (int i = 0; i < width; ++i)
      {
        if(line[i] == '.')
          temp2.push_back(0);
        else
          temp2.push_back(1);
      }
      v.push_back(temp2);
    }
    myfile.close();
  }
}

void extract(vector< vector<int> > &vin,vector< vector<int> > &vout,int R,int C,int C_size,int R_size) {
  for (int i = R; i < R+R_size; ++i)
  {
    vector<int> temp(vin[i].begin()+C,vin[i].begin()+C+C_size);
    vout.push_back(temp);
  }
}

int main(int argc, char const *argv[])
{
  vector< vector<int> > v;
  int length_bla;

  // Read input
  read_input(argv[1],v);

  ifstream ifs ("output.txt", ifstream::in);
  if (ifs) {
    // get length of file:
    ifs.seekg (0, ifs.end);
    length_bla = ifs.tellg();
    ifs.seekg (0, ifs.beg);
  }
  ifs.close();

  ofstream ofs ("output.txt", ofstream::out);
  ofs.close();

  return 0;
}