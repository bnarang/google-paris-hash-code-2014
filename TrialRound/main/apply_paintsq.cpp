#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "functions.hpp"

using namespace std;


void extract(vector< vector<int> > &vin,vector< vector<int> > &vout,int R,int C,int C_size,int R_size) {
  for (int i = R; i < R+R_size; ++i)
  {
    vector<int> temp(vin[i].begin()+C,vin[i].begin()+C+C_size);
    vout.push_back(temp);
  }
}


// dit si le carre est vide ou pas
bool est_vide(vector<vector <int> > carre) {
  int N = carre.size();
  if (carre[0].size() != N) {
    cout << "ERREUR : n'est pas un carré." << endl;
    return false;
  }

  for (int r = 0; r < N; ++r)
  {
    for (int c = 0; c < N; ++c)
      if (carre[r][c] == 1)
        return true;
  }

  return true;
}

void apply_paintsq(int S, vector<vector <int> > image)
{
  int R_max = image.size();
  int C_max = image[0].size();
  int R_min = 0;
  int C_min = 0;

  vector<vector <int> > tmp;

  // D'abord, on se déplace vers le bas, jusqu'à trouver la première ligne non vide
  vector<vector <int> > ligne;
  extract(image, ligne, 0, 0, C_max, 1); // première ligne
  while( est_vide(ligne) ) {
    R_min++;
    extract(image, ligne, 0, R_min, C_max, 1);
  }

  // On se décale vers la droite
  vector<vector <int> > colonne;
  extract(image, ligne, 0, 0, 1, R_max); // première ligne
  while( est_vide(colonne) ) {
    C_min++;
    extract(image, colonne, C_min, 0, 1, R_max);
  }


  for (int r = R_min; r < R_max; r += S)
  {
    for (int c = C_min; c < C_max; c += S)
    {
      extract(image, tmp, r, c, S, S);
      if ( !est_vide(image) )
      {
        paintsq(r, c, S, image);
      }
    }
  }
}
