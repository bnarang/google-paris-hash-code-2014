#include "functions.hpp"
#include <fstream>

using namespace std;

void output(vector<int> data) {

  int size = data.size();

  ofstream ofs ("output.txt", ofstream::out);

  if(size == 2) {
    ofs << "PAINTSQ " << data[0] << " " << data[1] << " " << data[2] << "\n";
  }

  if(size == 3) {
    ofs << "ERASECELL " << data[0] << " " << data[1] << "\n";
  }

  if(size != 2 && size != 3) {
    fprintf(stderr, "data de mauvaise taille\n");
  }

  ofs.close();
}