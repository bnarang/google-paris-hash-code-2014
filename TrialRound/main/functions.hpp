#ifndef FUNCTIONS_HPP
#define FUNCTIONS_HPP

#include <vector>
#include <iostream>

using namespace std;

// Fonction qui fait PAINTSQ R C S
void paintsq(int, int, int, vector<vector <int> >);

// Fonction qui fait ERASE R C
void erasecell(int, int, vector<vector <int> >);

// Fonction qui écrit les instructions dans le fichier d'output
void write_instr(vector<vector <int> >);

// Fonction qui applique le pinceau de taille N sur image (naïf)
void apply_paintsq(int taille_pinceau, vector<vector <int> > image);

// Fonction qui écrit l'output
void output(vector<int> data);

#endif
