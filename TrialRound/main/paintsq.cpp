#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "functions.hpp"

using namespace std;


void paintsq(int R, int C, int S, vector<vector <int> > image)
{
  vector<int> instuction;
  instuction.push_back(R);
  instuction.push_back(C);
  instuction.push_back(S);
  output(instuction);

  for (int r = R-S; r <= R+S; ++r)
  {
    for (int c = C-S; c <= C+S; ++c)
    {
      if (image[r][c] == 0)
        image[r][c] = 1;
    }
  }
}
