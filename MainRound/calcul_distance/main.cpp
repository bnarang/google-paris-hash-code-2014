#include <boost/algorithm/string.hpp>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stdio.h>      /* printf, scanf, puts, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */


using namespace std;
using namespace boost;

int distance_totale(const char* fichier_resultats, vector<vector <int> > &input)
{
  vector<int> couple_input;
  vector<int> couple_tmp, couple_tmp_rev;
  string line;
  ifstream myfile (fichier_resultats);
  int N, M, distance = 0;

  if (myfile.is_open())
  {
    // Read 1st line
    // N = nombre de véhicules
    getline(myfile, line);
    N = atoi(line.c_str());

    // Read for N vehicules
    for (int i = 0; i < N; ++i)
    {
      // Read for vehicule i

      // First line : M = number of nodes
      getline(myfile, line);
      M = atoi(line.c_str());

      // On boucle sur les nœuds parcourus
      // On regarde le premier nœud
      getline(myfile, line);
      int node1 = atoi(line.c_str());
      int node2;
      for (int j = 1; j < M; ++j)
      {
        getline(myfile, line);
        node2 = atoi(line.c_str());

        couple_tmp.clear();
        couple_tmp_rev.clear();
        couple_tmp.push_back(node1);
        couple_tmp.push_back(node2);
        couple_tmp_rev.push_back(node2);
        couple_tmp_rev.push_back(node1);

        for (int k = 0; k < input.size(); ++k)
        {
          couple_input.clear();
          couple_input.push_back(input[k][0]);
          couple_input.push_back(input[k][1]);
          if ( couple_input == couple_tmp ||
              (couple_input == couple_tmp_rev && input[k][2] == 2) )
          {
            distance += input[k][4];
            input[k][4] = 0;
          }
        }

        node1 = node2;
      }
    }

    myfile.close();
  }

  return distance;
}




void read_input(const char* filename,
                vector< vector<float> > &noeuds,
                vector< vector<int> > &arretes,
                int &N,
                int &M,
                int &T,
                int &C,
                int &S
                )
{
  vector<string> temp;
  vector<float> temp2;
  vector<int> temp3;
  string line;
  ifstream myfile (filename);

  if (myfile.is_open())
  {
    // Read 1st line
    getline (myfile,line);
    split( temp, line, is_any_of( " " ), token_compress_on );

    N = atoi(temp[0].c_str());
    M = atoi(temp[1].c_str());
    T = atoi(temp[2].c_str());
    C = atoi(temp[3].c_str());
    S = atoi(temp[4].c_str());

    // Read N nodes lines
    for (int i = 0; i < N; ++i)
    {
      getline (myfile,line);
      split( temp, line, is_any_of( " " ), token_compress_on );
      temp2.clear();
      temp2.push_back(atof(temp[0].c_str()));
      temp2.push_back(atof(temp[1].c_str()));
      noeuds.push_back(temp2);
    }
    // Read M arretes lines
    for (int i = 0; i < M; ++i)
    {
      getline (myfile,line);
      split( temp, line, is_any_of( " " ), token_compress_on );
      temp3.clear();
      temp3.push_back(atof(temp[0].c_str()));
      temp3.push_back(atof(temp[1].c_str()));
      temp3.push_back(atof(temp[2].c_str()));
      temp3.push_back(atof(temp[3].c_str()));
      temp3.push_back(atof(temp[4].c_str()));
      arretes.push_back(temp3);
    }

    myfile.close();
  }
}




/*********************************/
int main(int argc, char const *argv[])
{
  if (argc != 3) {
    fprintf(stderr, "T'as pas le bon nombre d'argument pôv' chèvre !\n");
    exit (-1);
  }

  vector< vector<float> > noeuds;
  vector< vector<int> > arretes;
  int N,M,T,C,S;

  // Read input
  read_input(argv[1], noeuds, arretes, N, M, T, C, S);
  int distance = distance_totale(argv[2], arretes);

  cout << distance << endl;

  return 0;
}
