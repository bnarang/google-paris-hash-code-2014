#ifndef FUNCTIONS_HPP
#define FUNCTIONS_HPP
#include <vector>
#include <string>

using namespace std;

void output(vector<vector<int> > data, const char* fileName);

#endif