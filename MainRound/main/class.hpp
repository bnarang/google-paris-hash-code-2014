#ifndef __VEHICULE__HPP__
#define __VEHICULE__HPP__

extern int tempsRestant;
#include <sys/types.h>
#include <unistd.h>

class Node {
  private:
    int          index;
    vector<Node> children;
    vector<int>  times;
    vector<int>  lengths;
    vector<int>  proba;
    vector<bool> path_taken;

  public:
    Node() { index = 0; }
    Node(int theIndex) { index = theIndex; }

    int          getIndex()    { return index;    }
    vector<Node> getChildren() { return children; }
    vector<int>  getTimes()    { return times;    }
    vector<int>  getLengths()  { return lengths;  }
    vector<int>  getProbas()   { return proba;    }

    void setPathTaken() { path_taken = true; }

    void addChild(Node theChild, int theTime, int theLength)
    {
      children.push_back(theChild);
      times.push_back(theTime);
      lengths.push_back(theLength);
      proba.push_back(10000000);
      path_taken.push_back(false);
    }

    /* si deja parcouru */
    void proba_1(int indexNextChild, int param) {
      proba[indexNextChild] -= param;
    }

    /* si sens unique */
    // void proba_2() {
    //   for (int i = 0; i < children.size(); ++i)
    //   {
    //     vector<Node> childrenFromChild = children[i].getChildren();
    //     for (int j = 0; j < childrenFromChild.size(); ++j)
    //     {
    //       if (childrenFromChild[j] == this) {
    //         proba[i] -= 10;
    //         break;
    //       }
    //     }
    //   }
    // }

    /* si pas assez de temps prob = 0 */
    void proba_4() {
      for (int i = 0; i < children.size(); ++i)
      {
        if (tempsRestant < times[i])
          proba[i] = 0;
      }
    }

    void proba_3(int param)
    {
      // 10 * Li/ Ci
      for (int i = 0; i < children.size(); ++i)
        proba[i] += 4 * param * lengths[i] / times[i];
    }

};

class Vehicule {
private:
  int vehicule;
  int intersection;
  int formerIntersection;
  int spentTime;
  vector<Node> tree;

 public:
  Vehicule(int number, int start, vector<Node> _tree){
    vehicule = number;
    intersection = start;
    tree = _tree;
    formerIntersection = start;
    spentTime = 0;
  }

  int getCurrentIntersection()     { return intersection; }
  int getFormerIntersection()      { return formerIntersection; }
  int getVehicule()                { return vehicule; }
  int getSpentTime()               { return spentTime; }

  void setIntersection(int n)      { intersection = n; }
  void setFormerIntersection(int n){ formerIntersection = n; }
  void setTime(int n)              { spentTime = n; }

  void goNext(int param1) {
    //int n = rand() % tree[intersection].getChildren().size();
    // Détermination des lieux favorables si plus assez de temps
    //tree[intersection].proba_4();
    int sum=0;
    for (int j=0; j<tree[intersection].getChildren().size();j++){
      sum += tree[intersection].getProbas()[j];
    }
    int m = rand() % sum;
    int n=0;
    int acc=tree[intersection].getProbas()[0];
    while ( m > acc ){
      n++;
      acc += tree[intersection].getProbas()[n];
    }
    formerIntersection = intersection;
    intersection = tree[intersection].getChildren()[n].getIndex();
    spentTime += tree[formerIntersection].getTimes()[n];

    // incrément distance totale
    if (!path_taken) {
      tree[formerIntersection].setPathTaken();
      distance_totale += tree[formerIntersection]
    }

    // actualisation des probas
    tree[formerIntersection].proba_1(n, param1);
  }

  bool isEmpty(){
      for (int i=0; i< tree[intersection].getProbas().size(); i++){
        if (tree[intersection].getProbas()[i] != 0){
          return false;
        }
      }
      return true;
    }

 };

#endif