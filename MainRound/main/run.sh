#!/bin/bash
NB_PROCESS=1
INPUT_FILE="paris_54000.txt"
OUTPUT_FILE=""

for i in $(seq 1 $NB_PROCESS); 
do
    for p1 in $(seq 10 5 50);
    do
        for p2 in $(seq 10 5 50);
        do
            OUTPUT_FILE=$(date +%j-%T)_$i_$p1_$p2
            ./a.out $INPUT_FILE $OUTPUT_FILE $p1 $p2 &
        done
    done
    #sleep 1
done;