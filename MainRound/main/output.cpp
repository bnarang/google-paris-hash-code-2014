#include "functions.hpp"
#include <fstream>
#include <ctime>
#include <string>

using namespace std;

void output(vector<vector<int> > data, const char* fileName) {

  int nbCars = data.size();

  ofstream ofs (fileName, ofstream::out);

  ofs << nbCars << "\n";

  for (int i = 0; i < nbCars; i++){
    ofs << data[i].size() << "\n";

    for (int j = 0; j < data[i].size(); j++){
      ofs << data[i][j] << "\n";
    }
  }

  ofs.close();
}