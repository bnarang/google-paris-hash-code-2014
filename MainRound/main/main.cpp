/*
 *         ####  ####
 *        ####  ####
 *    ################
 *   ###############
 *     ####  ####
 *  ###############
 * ###############
 *   ####  ####
 *  ####  ####
 *
 * Google Paris Hash Code - 5 avril 2014
 *
 * Team ENSTAckoverflow
 * Quentin B. - Sylvain F. - Adrien L. - Benjamin N.
 *
 */

#include <boost/algorithm/string.hpp>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "functions.hpp"
#include "class.hpp"
#include <stdio.h>      /* printf, scanf, puts, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <sys/types.h>
#include <unistd.h>


using namespace std;
using namespace boost;

int tempsRestant;
int distance_totale;

void read_input(const char* filename,
                vector< vector<float> > &noeuds,
                vector< vector<int> > &arretes,
                int &N,
                int &M,
                int &T,
                int &C,
                int &S
                )
{
  vector<string> temp;
  vector<float> temp2;
  vector<int> temp3;
  string line;
  ifstream myfile (filename);

  if (myfile.is_open())
  {
    // Read 1st line
    getline (myfile,line);
    split( temp, line, is_any_of( " " ), token_compress_on );

    N = atoi(temp[0].c_str());
    M = atoi(temp[1].c_str());
    T = atoi(temp[2].c_str());
    C = atoi(temp[3].c_str());
    S = atoi(temp[4].c_str());

    // Read N nodes lines
    for (int i = 0; i < N; ++i)
    {
      getline (myfile,line);
      split( temp, line, is_any_of( " " ), token_compress_on );
      temp2.clear();
      temp2.push_back(atof(temp[0].c_str()));
      temp2.push_back(atof(temp[1].c_str()));
      noeuds.push_back(temp2);
    }
    // Read M arretes lines
    for (int i = 0; i < M; ++i)
    {
      getline (myfile,line);
      split( temp, line, is_any_of( " " ), token_compress_on );
      temp3.clear();
      temp3.push_back(atof(temp[0].c_str()));
      temp3.push_back(atof(temp[1].c_str()));
      temp3.push_back(atof(temp[2].c_str()));
      temp3.push_back(atof(temp[3].c_str()));
      temp3.push_back(atof(temp[4].c_str()));
      arretes.push_back(temp3);
    }

    myfile.close();
  }
}

void read_input_test_print(
                            vector< vector<float> > noeuds,
                            vector< vector<int> > arretes,
                            int N,
                            int M,
                            int T,
                            int C,
                            int S
                            )
{
  cout << "Temps : " << T << endl << "Véhicules : " << C << endl << "Init position : " << S << endl;
  cout << "Nodes  (" << N << ")" << endl;
  for (int i = 0; i < noeuds.size(); ++i)
  {
    for (int j = 0; j < noeuds[i].size(); ++j)
    {
      cout << noeuds[i][j] << "-";
    }
    cout << endl;
  }

  cout << endl << "Arretes (" << M << ")" << endl;
  for (int i = 0; i < arretes.size(); ++i)
  {
    for (int j = 0; j < arretes[i].size(); ++j)
    {
      cout << arretes[i][j] << "-";
    }
    cout << endl;
  }


}

int main(int argc, char const *argv[])
{
  if (argc != 5) {
    fprintf(stderr, "T'as pas le bon nombre d'argument pôv' chèvre !\n");
    exit (-1);
  }
  vector< vector<float> > noeuds;
  vector< vector<int> > arretes;
  int N,M,T,C,S;

  distance_totale = 0;

  srand(time(NULL));
  int param1 = atoi(argv[3]);
  int param2 = atoi(argv[4]);

  // Read input
  read_input(argv[1],noeuds,arretes,N,M,T,C,S);
  vector<Node> nodes;

  vector<Vehicule> vehicules;

  /***************************/
  // INITIALISATION DE L'ARBRE
  /***************************/
  vector<Node> tree;

  // On commence par créer les N nœuds
  for (int i = 0; i < N; ++i)
  {
    Node tmp = Node(i);
    tree.push_back(tmp);
  }

  // Ensuite, on les relit entre eux
  int theTime, theLength, sens_parcours;
  for (int j = 0; j < arretes.size(); ++j)
  {
    theTime   = arretes[j][3];
    theLength = arretes[j][4];
    tree[ arretes[j][0] ].addChild(tree[ arretes[j][1] ], theTime, theLength);

    // Si on est en double sens, on ajout aussi A à B
    sens_parcours = arretes[j][2];
    if (sens_parcours == 2)
      tree[ arretes[j][1] ].addChild(tree[ arretes[j][0] ], theTime, theLength);
  }

  Node root = tree[ arretes[0][0] ];

  /***************************/
  // INITIALISATION DES VEHICULES
  /***************************/
  vector<vector<int> > parcours;
  vector<int> parcours_temp;
  for (int i = 0; i < C; ++i)
    vehicules.push_back(Vehicule(i, S, tree));

 for (int i = 0; i < tree.size(); ++i)
 {
   tree[i].proba_3(param2);
 }

  for (int i = 0; i < C; ++i)
  {
    tempsRestant = 0;
    parcours_temp.clear();
    parcours_temp.push_back(S);
    while(tempsRestant >= 0) {
      tempsRestant = T-vehicules[i].getSpentTime();
      vehicules[i].goNext(param1);
      if(vehicules[i].getSpentTime() < T)
        parcours_temp.push_back(vehicules[i].getCurrentIntersection());
    }
    parcours.push_back(parcours_temp);
  }

  output(parcours,argv[2]);

  cout << distance_totale << endl;

  return 0;
}
